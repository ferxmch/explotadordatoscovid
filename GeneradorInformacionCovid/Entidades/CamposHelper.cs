﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorInformacionCovid.Entidades
{
    public class CamposHelper
    {
        public string FECHA_ACTUALIZACION { get; set; }
        public string ID_REGISTRO { get; set; }
        //public string ORIGEN { get; set; }
        public string SECTOR { get; set; }
        public string ENTIDAD_UM { get; set; }
        public string SEXO { get; set; }
        //public string ENTIDAD_NAC { get; set; }
        public string ENTIDAD_RES { get; set; }
        public string MUNICIPIO_RES { get; set; }
        public string TIPO_PACIENTE { get; set; }
        public string FECHA_INGRESO { get; set; }
        public string FECHA_SINTOMAS { get; set; }
        public string FECHA_DEF { get; set; }
        public string INTUBADO { get; set; }
        public string NEUMONIA { get; set; }
        public string EDAD { get; set; }
        //public string NACIONALIDAD { get; set; }
        public string EMBARAZO { get; set; }
        //public string HABLA_LENGUA_INDIG { get; set; }
        //public string INDIGENA { get; set; }
        public string DIABETES { get; set; }
        public string EPOC { get; set; }
        public string ASMA { get; set; }
        public string INMUSUPR { get; set; }
        public string HIPERTENSION { get; set; }
        public string OTRA_COM { get; set; }
        public string CARDIOVASCULAR { get; set; }
        public string OBESIDAD { get; set; }
        public string RENAL_CRONICA { get; set; }
        public string TABAQUISMO { get; set; }
        public string OTRO_CASO { get; set; }
        public string TOMA_MUESTRA_LAB { get; set; }
        public string RESULTADO_LAB { get; set; }
        public string TOMA_MUESTRA_ANTIGENO { get; set; }
        public string RESULTADO_ANTIGENO { get; set; }
        public string CLASIFICACION_FINAL { get; set; }
        //public string MIGRANTE { get; set; }
        //public string PAIS_NACIONALIDAD { get; set; }
        //public string PAIS_ORIGEN { get; set; }
        public string UCI { get; set; }
    }
}
