﻿using GeneradorInformacionCovid.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneradorFormApp
{
    public partial class Form1 : Form
    {
        NegocioLectura _Business;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBuscarFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|csv files (*.csv*)|*.csv";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtSelectFile.Text = openFileDialog.FileName;
                    _Business = new NegocioLectura(txtSelectFile.Text);
                    dgvDatosLeidos.DataSource = _Business.ObtenerPrevia();
                }
            }
        }

        private void btnSeleccionarRutaSalida_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtSelectSalida.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtSelectFile.Text)||string.IsNullOrEmpty(txtSelectSalida.Text))
            {
                MessageBox.Show("Debe seleccionar un archivo de lectura y una ruta de salida");
            }
            else
            {
                lblLectura.Text = "Generando Reporte";
                string salida = Path.Combine(txtSelectSalida.Text, "ResumenCovid" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".csv");                
                dgvDatosLeidos.DataSource = null;
                dgvDatosLeidos.DataSource = _Business.GenerarArchivoSalida(dtPicker.Value, salida);
                _Business.Dispose();
                lblLectura.Text = "Reporte Finalizado";
            }
        }
    }
}
