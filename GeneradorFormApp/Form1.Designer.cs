﻿
namespace GeneradorFormApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSeleccione = new System.Windows.Forms.Label();
            this.lblRutaSalida = new System.Windows.Forms.Label();
            this.txtSelectFile = new System.Windows.Forms.TextBox();
            this.txtSelectSalida = new System.Windows.Forms.TextBox();
            this.btnBuscarFile = new System.Windows.Forms.Button();
            this.btnSeleccionarRutaSalida = new System.Windows.Forms.Button();
            this.dgvDatosLeidos = new System.Windows.Forms.DataGridView();
            this.btnExportar = new System.Windows.Forms.Button();
            this.dtPicker = new System.Windows.Forms.DateTimePicker();
            this.lblLectura = new System.Windows.Forms.Label();
            this.lblFechaSintoma = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosLeidos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSeleccione
            // 
            this.lblSeleccione.AutoSize = true;
            this.lblSeleccione.Location = new System.Drawing.Point(26, 27);
            this.lblSeleccione.Name = "lblSeleccione";
            this.lblSeleccione.Size = new System.Drawing.Size(99, 13);
            this.lblSeleccione.TabIndex = 0;
            this.lblSeleccione.Text = "Seleccione Archivo";
            // 
            // lblRutaSalida
            // 
            this.lblRutaSalida.AutoSize = true;
            this.lblRutaSalida.Location = new System.Drawing.Point(26, 76);
            this.lblRutaSalida.Name = "lblRutaSalida";
            this.lblRutaSalida.Size = new System.Drawing.Size(133, 13);
            this.lblRutaSalida.TabIndex = 1;
            this.lblRutaSalida.Text = "Seleccione Ruta de Salida";
            // 
            // txtSelectFile
            // 
            this.txtSelectFile.Enabled = false;
            this.txtSelectFile.Location = new System.Drawing.Point(29, 44);
            this.txtSelectFile.Name = "txtSelectFile";
            this.txtSelectFile.Size = new System.Drawing.Size(493, 20);
            this.txtSelectFile.TabIndex = 2;
            // 
            // txtSelectSalida
            // 
            this.txtSelectSalida.Enabled = false;
            this.txtSelectSalida.Location = new System.Drawing.Point(29, 92);
            this.txtSelectSalida.Name = "txtSelectSalida";
            this.txtSelectSalida.Size = new System.Drawing.Size(493, 20);
            this.txtSelectSalida.TabIndex = 3;
            // 
            // btnBuscarFile
            // 
            this.btnBuscarFile.Location = new System.Drawing.Point(541, 44);
            this.btnBuscarFile.Name = "btnBuscarFile";
            this.btnBuscarFile.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarFile.TabIndex = 4;
            this.btnBuscarFile.Text = "Seleccionar";
            this.btnBuscarFile.UseVisualStyleBackColor = true;
            this.btnBuscarFile.Click += new System.EventHandler(this.btnBuscarFile_Click);
            // 
            // btnSeleccionarRutaSalida
            // 
            this.btnSeleccionarRutaSalida.Location = new System.Drawing.Point(541, 92);
            this.btnSeleccionarRutaSalida.Name = "btnSeleccionarRutaSalida";
            this.btnSeleccionarRutaSalida.Size = new System.Drawing.Size(75, 23);
            this.btnSeleccionarRutaSalida.TabIndex = 5;
            this.btnSeleccionarRutaSalida.Text = "Seleccionar";
            this.btnSeleccionarRutaSalida.UseVisualStyleBackColor = true;
            this.btnSeleccionarRutaSalida.Click += new System.EventHandler(this.btnSeleccionarRutaSalida_Click);
            // 
            // dgvDatosLeidos
            // 
            this.dgvDatosLeidos.AllowUserToAddRows = false;
            this.dgvDatosLeidos.AllowUserToDeleteRows = false;
            this.dgvDatosLeidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosLeidos.Location = new System.Drawing.Point(29, 185);
            this.dgvDatosLeidos.Name = "dgvDatosLeidos";
            this.dgvDatosLeidos.ReadOnly = true;
            this.dgvDatosLeidos.Size = new System.Drawing.Size(731, 292);
            this.dgvDatosLeidos.TabIndex = 6;
            // 
            // btnExportar
            // 
            this.btnExportar.Location = new System.Drawing.Point(541, 135);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(75, 23);
            this.btnExportar.TabIndex = 7;
            this.btnExportar.Text = "Generar Salida";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // dtPicker
            // 
            this.dtPicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPicker.Location = new System.Drawing.Point(29, 135);
            this.dtPicker.Name = "dtPicker";
            this.dtPicker.Size = new System.Drawing.Size(200, 20);
            this.dtPicker.TabIndex = 8;
            // 
            // lblLectura
            // 
            this.lblLectura.AutoSize = true;
            this.lblLectura.Location = new System.Drawing.Point(29, 162);
            this.lblLectura.Name = "lblLectura";
            this.lblLectura.Size = new System.Drawing.Size(55, 13);
            this.lblLectura.TabIndex = 9;
            this.lblLectura.Text = "En espera";
            // 
            // lblFechaSintoma
            // 
            this.lblFechaSintoma.AutoSize = true;
            this.lblFechaSintoma.Location = new System.Drawing.Point(26, 119);
            this.lblFechaSintoma.Name = "lblFechaSintoma";
            this.lblFechaSintoma.Size = new System.Drawing.Size(93, 13);
            this.lblFechaSintoma.TabIndex = 10;
            this.lblFechaSintoma.Text = "Fecha de Sintoma";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 489);
            this.Controls.Add(this.lblFechaSintoma);
            this.Controls.Add(this.lblLectura);
            this.Controls.Add(this.dtPicker);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.dgvDatosLeidos);
            this.Controls.Add(this.btnSeleccionarRutaSalida);
            this.Controls.Add(this.btnBuscarFile);
            this.Controls.Add(this.txtSelectSalida);
            this.Controls.Add(this.txtSelectFile);
            this.Controls.Add(this.lblRutaSalida);
            this.Controls.Add(this.lblSeleccione);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.Text = "Generar Resumen COVID";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosLeidos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSeleccione;
        private System.Windows.Forms.Label lblRutaSalida;
        private System.Windows.Forms.TextBox txtSelectFile;
        private System.Windows.Forms.TextBox txtSelectSalida;
        private System.Windows.Forms.Button btnBuscarFile;
        private System.Windows.Forms.Button btnSeleccionarRutaSalida;
        private System.Windows.Forms.DataGridView dgvDatosLeidos;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.DateTimePicker dtPicker;
        private System.Windows.Forms.Label lblLectura;
        private System.Windows.Forms.Label lblFechaSintoma;
    }
}

