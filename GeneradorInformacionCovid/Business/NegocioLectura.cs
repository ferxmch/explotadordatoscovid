﻿using GeneradorInformacionCovid.Entidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorInformacionCovid.Business
{
    public class NegocioLectura : IDisposable
    {
        private StreamWriter _Sw;
        private DateTime _FechaElegida;
        private IEnumerable<string> _LineasArchivos;
        public NegocioLectura(string RutaArchivo)
        {
            _LineasArchivos = File.ReadLines(RutaArchivo);
        }

        public List<CamposHelper> ObtenerPrevia()
        {
            List<CamposHelper> Lineas = new List<CamposHelper>();
            int contador = 0;
            foreach (var a in _LineasArchivos.Take(21))
            {
                if (contador == 0)
                {
                    contador++;
                    continue;
                }
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                Caso.DIABETES = spliteado[20].Replace("\"", "");
                Caso.EPOC = spliteado[21].Replace("\"", "");
                Caso.ASMA = spliteado[22].Replace("\"", "");
                Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Lineas.Add(Caso);
            }
            return Lineas;
        }





        public List<CamposHelper> GenerarArchivoSalida(DateTime FechaElegida, string RutaArchivoSalida)
        {
            List<CamposHelper> Lineas = new List<CamposHelper>();
            _FechaElegida = FechaElegida;
            _Sw = new StreamWriter(RutaArchivoSalida);
            int contador = 0;
            foreach (var a in _LineasArchivos)
            {
                var spliteado = a.Split(',');
                try
                {
                    var fecha = Convert.ToDateTime(spliteado[10].Replace("\"", ""));
                    if (fecha <= _FechaElegida)
                        continue;
                }
                catch (Exception ex)
                {

                }
                if (contador <= 21 && contador >0)
                {

                    CamposHelper Caso = new CamposHelper();
                    Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                    Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                    Caso.SECTOR = spliteado[3].Replace("\"", "");
                    Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                    Caso.SEXO = spliteado[5].Replace("\"", "");
                    Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                    Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                    Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                    Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                    Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                    Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                    Caso.INTUBADO = spliteado[13].Replace("\"", "");
                    Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                    Caso.EDAD = spliteado[15].Replace("\"", "");
                    Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                    Caso.DIABETES = spliteado[20].Replace("\"", "");
                    Caso.EPOC = spliteado[21].Replace("\"", "");
                    Caso.ASMA = spliteado[22].Replace("\"", "");
                    Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                    Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                    Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                    Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                    Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                    Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                    Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                    Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                    Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                    Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                    Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                    Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                    Caso.UCI = spliteado[39].Replace("\"", "");
                    Lineas.Add(Caso);
                }

                contador++;
                _Sw.WriteLine(spliteado[0].Replace("\"", "") + "," +
                             spliteado[1].Replace("\"", "") + "," +
                             spliteado[3].Replace("\"", "") + "," +
                             spliteado[4].Replace("\"", "") + "," +
                             spliteado[5].Replace("\"", "") + "," +
                             spliteado[7].Replace("\"", "") + "," +
                             spliteado[8].Replace("\"", "") + "," +
                             spliteado[9].Replace("\"", "") + "," +
                             spliteado[10].Replace("\"", "") + "," +
                             spliteado[11].Replace("\"", "") + "," +
                             spliteado[12].Replace("\"", "") + "," +
                             spliteado[13].Replace("\"", "") + "," +
                             spliteado[14].Replace("\"", "") + "," +
                             spliteado[15].Replace("\"", "") + "," +
                             spliteado[17].Replace("\"", "") + "," +
                             spliteado[20].Replace("\"", "") + "," +
                             spliteado[21].Replace("\"", "") + "," +
                             spliteado[22].Replace("\"", "") + "," +
                             spliteado[23].Replace("\"", "") + "," +
                             spliteado[24].Replace("\"", "") + "," +
                             spliteado[25].Replace("\"", "") + "," +
                             spliteado[26].Replace("\"", "") + "," +
                             spliteado[27].Replace("\"", "") + "," +
                             spliteado[28].Replace("\"", "") + "," +
                             spliteado[29].Replace("\"", "") + "," +
                             spliteado[31].Replace("\"", "") + "," +
                             spliteado[32].Replace("\"", "") + "," +
                             spliteado[33].Replace("\"", "") + "," +
                             spliteado[34].Replace("\"", "") + "," +
                             spliteado[35].Replace("\"", "") + "," +
                             spliteado[39].Replace("\"", ""));
            }
            return Lineas;
        }


        public void Dispose()
        {
            _LineasArchivos = null;
            if (_Sw != null)
            {
                _Sw.Close();
                _Sw.Dispose();
            }
        }
    }
}
