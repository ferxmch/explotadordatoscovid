﻿using GeneradorInformacionCovid.Entidades;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorTest
{
    [TestClass]
    public class UnitTest2
    {

        [TestMethod]
        public void TestPrueba0()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                Caso.DIABETES = spliteado[20].Replace("\"", "");
                Caso.EPOC = spliteado[21].Replace("\"", "");
                Caso.ASMA = spliteado[22].Replace("\"", "");
                Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                t.SEXO,
                t.ENTIDAD_RES,
                t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                t.EMBARAZO,
                t.DIABETES,
                t.EPOC,
                t.ASMA,
                t.INMUSUPR,
                t.HIPERTENSION,
                t.OTRA_COM,
                t.CARDIOVASCULAR,
                t.OBESIDAD,
                t.RENAL_CRONICA,
                t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_0.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count()+"|"+
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                a.Key.EMBARAZO + "|" +
                a.Key.DIABETES + "|" +
                a.Key.EPOC + "|" +
                a.Key.ASMA + "|" +
                a.Key.INMUSUPR + "|" +
                a.Key.HIPERTENSION + "|" +
                a.Key.OTRA_COM + "|" +
                a.Key.CARDIOVASCULAR + "|" +
                a.Key.OBESIDAD + "|" +
                a.Key.RENAL_CRONICA + "|" +
                a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_0.txt", "Total de lineas: " + grupos.Count());
        }


        [TestMethod]
        public void TestPrueba1()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                //Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                Caso.DIABETES = spliteado[20].Replace("\"", "");
                Caso.EPOC = spliteado[21].Replace("\"", "");
                Caso.ASMA = spliteado[22].Replace("\"", "");
                Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                //t.SEXO,
                t.ENTIDAD_RES,
                t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                t.EMBARAZO,
                t.DIABETES,
                t.EPOC,
                t.ASMA,
                t.INMUSUPR,
                t.HIPERTENSION,
                t.OTRA_COM,
                t.CARDIOVASCULAR,
                t.OBESIDAD,
                t.RENAL_CRONICA,
                t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_1.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count() + "|" +
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                //a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                a.Key.EMBARAZO + "|" +
                a.Key.DIABETES + "|" +
                a.Key.EPOC + "|" +
                a.Key.ASMA + "|" +
                a.Key.INMUSUPR + "|" +
                a.Key.HIPERTENSION + "|" +
                a.Key.OTRA_COM + "|" +
                a.Key.CARDIOVASCULAR + "|" +
                a.Key.OBESIDAD + "|" +
                a.Key.RENAL_CRONICA + "|" +
                a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_1.txt", "Total de lineas: " + grupos.Count());
        }


        [TestMethod]
        public void TestPrueba2()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                //Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                Caso.DIABETES = spliteado[20].Replace("\"", "");
                Caso.EPOC = spliteado[21].Replace("\"", "");
                Caso.ASMA = spliteado[22].Replace("\"", "");
                Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                t.SEXO,
                t.ENTIDAD_RES,
                //t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                t.EMBARAZO,
                t.DIABETES,
                t.EPOC,
                t.ASMA,
                t.INMUSUPR,
                t.HIPERTENSION,
                t.OTRA_COM,
                t.CARDIOVASCULAR,
                t.OBESIDAD,
                t.RENAL_CRONICA,
                t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_2.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count() + "|" +
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                //a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                a.Key.EMBARAZO + "|" +
                a.Key.DIABETES + "|" +
                a.Key.EPOC + "|" +
                a.Key.ASMA + "|" +
                a.Key.INMUSUPR + "|" +
                a.Key.HIPERTENSION + "|" +
                a.Key.OTRA_COM + "|" +
                a.Key.CARDIOVASCULAR + "|" +
                a.Key.OBESIDAD + "|" +
                a.Key.RENAL_CRONICA + "|" +
                a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_2.txt", "Total de lineas: " + grupos.Count());
        }


        [TestMethod]
        public void TestPrueba4()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                //Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                //Caso.DIABETES = spliteado[20].Replace("\"", "");
                //Caso.EPOC = spliteado[21].Replace("\"", "");
                //Caso.ASMA = spliteado[22].Replace("\"", "");
                //Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                //Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                //Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                //Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                //Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                //Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                //Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                t.SEXO,
                t.ENTIDAD_RES,
                t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                //t.EMBARAZO,
                //t.DIABETES,
                //t.EPOC,
                //t.ASMA,
                //t.INMUSUPR,
                //t.HIPERTENSION,
                //t.OTRA_COM,
                //t.CARDIOVASCULAR,
                //t.OBESIDAD,
                //t.RENAL_CRONICA,
                //t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_4.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count() + "|" +
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                //a.Key.EMBARAZO + "|" +
                //a.Key.DIABETES + "|" +
                //a.Key.EPOC + "|" +
                //a.Key.ASMA + "|" +
                //a.Key.INMUSUPR + "|" +
                //a.Key.HIPERTENSION + "|" +
                //a.Key.OTRA_COM + "|" +
                //a.Key.CARDIOVASCULAR + "|" +
                //a.Key.OBESIDAD + "|" +
                //a.Key.RENAL_CRONICA + "|" +
                //a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_4.txt", "Total de lineas: " + grupos.Count());
        }



        [TestMethod]
        public void TestPrueba5()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                //Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                //Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                //Caso.DIABETES = spliteado[20].Replace("\"", "");
                //Caso.EPOC = spliteado[21].Replace("\"", "");
                //Caso.ASMA = spliteado[22].Replace("\"", "");
                //Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                //Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                //Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                //Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                //Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                //Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                //Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                t.SEXO,
                t.ENTIDAD_RES,
                //t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                //t.EMBARAZO,
                //t.DIABETES,
                //t.EPOC,
                //t.ASMA,
                //t.INMUSUPR,
                //t.HIPERTENSION,
                //t.OTRA_COM,
                //t.CARDIOVASCULAR,
                //t.OBESIDAD,
                //t.RENAL_CRONICA,
                //t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_5.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count() + "|" +
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                //a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                //a.Key.EMBARAZO + "|" +
                //a.Key.DIABETES + "|" +
                //a.Key.EPOC + "|" +
                //a.Key.ASMA + "|" +
                //a.Key.INMUSUPR + "|" +
                //a.Key.HIPERTENSION + "|" +
                //a.Key.OTRA_COM + "|" +
                //a.Key.CARDIOVASCULAR + "|" +
                //a.Key.OBESIDAD + "|" +
                //a.Key.RENAL_CRONICA + "|" +
                //a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_5.txt", "Total de lineas: " + grupos.Count());
        }



        [TestMethod]
        public void TestPrueba6()
        {
            string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
            List<CamposHelper> Listado = new List<CamposHelper>();
            var lectura = File.ReadLines(Archivo);
            foreach (var a in lectura)
            {
                var spliteado = a.Split(',');
                CamposHelper Caso = new CamposHelper();
                Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
                Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
                Caso.SECTOR = spliteado[3].Replace("\"", "");
                Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
                Caso.SEXO = spliteado[5].Replace("\"", "");
                Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
                //Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
                Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
                Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
                Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
                Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
                Caso.INTUBADO = spliteado[13].Replace("\"", "");
                Caso.NEUMONIA = spliteado[14].Replace("\"", "");
                Caso.EDAD = spliteado[15].Replace("\"", "");
                //Caso.EMBARAZO = spliteado[17].Replace("\"", "");
                //Caso.DIABETES = spliteado[20].Replace("\"", "");
                //Caso.EPOC = spliteado[21].Replace("\"", "");
                //Caso.ASMA = spliteado[22].Replace("\"", "");
                //Caso.INMUSUPR = spliteado[23].Replace("\"", "");
                //Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
                //Caso.OTRA_COM = spliteado[25].Replace("\"", "");
                //Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
                //Caso.OBESIDAD = spliteado[27].Replace("\"", "");
                //Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
                //Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
                Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
                Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
                Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
                Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
                Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
                Caso.UCI = spliteado[39].Replace("\"", "");
                Listado.Add(Caso);
            }

            var grupos = Listado.GroupBy(t => new {
                t.FECHA_ACTUALIZACION,
                t.SECTOR,
                t.ENTIDAD_UM,
                t.SEXO,
                t.ENTIDAD_RES,
                //t.MUNICIPIO_RES,
                t.TIPO_PACIENTE,
                t.FECHA_INGRESO,
                t.FECHA_SINTOMAS,
                t.FECHA_DEF,
                t.INTUBADO,
                t.NEUMONIA,
                t.EDAD,
                //t.EMBARAZO,
                //t.DIABETES,
                //t.EPOC,
                //t.ASMA,
                //t.INMUSUPR,
                //t.HIPERTENSION,
                //t.OTRA_COM,
                //t.CARDIOVASCULAR,
                //t.OBESIDAD,
                //t.RENAL_CRONICA,
                //t.TABAQUISMO,
                t.TOMA_MUESTRA_LAB,
                t.RESULTADO_LAB,
                t.TOMA_MUESTRA_ANTIGENO,
                t.RESULTADO_ANTIGENO,
                t.CLASIFICACION_FINAL,
                t.UCI
            });

            StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_6.csv");
            int contador = 0;
            foreach (var a in grupos)
            {
                contador++;
                sw.WriteLine(
                    a.Count() + "|" +
                a.Key.FECHA_ACTUALIZACION + "|" +
                a.Key.SECTOR + "|" +
                a.Key.ENTIDAD_UM + "|" +
                a.Key.SEXO + "|" +
                a.Key.ENTIDAD_RES + "|" +
                //a.Key.MUNICIPIO_RES + "|" +
                a.Key.TIPO_PACIENTE + "|" +
                a.Key.FECHA_INGRESO + "|" +
                a.Key.FECHA_SINTOMAS + "|" +
                a.Key.FECHA_DEF + "|" +
                a.Key.INTUBADO + "|" +
                a.Key.NEUMONIA + "|" +
                a.Key.EDAD + "|" +
                //a.Key.EMBARAZO + "|" +
                //a.Key.DIABETES + "|" +
                //a.Key.EPOC + "|" +
                //a.Key.ASMA + "|" +
                //a.Key.INMUSUPR + "|" +
                //a.Key.HIPERTENSION + "|" +
                //a.Key.OTRA_COM + "|" +
                //a.Key.CARDIOVASCULAR + "|" +
                //a.Key.OBESIDAD + "|" +
                //a.Key.RENAL_CRONICA + "|" +
                //a.Key.TABAQUISMO + "|" +
                a.Key.TOMA_MUESTRA_LAB + "|" +
                a.Key.RESULTADO_LAB + "|" +
                a.Key.TOMA_MUESTRA_ANTIGENO + "|" +
                a.Key.RESULTADO_ANTIGENO + "|" +
                a.Key.CLASIFICACION_FINAL + "|" +
                a.Key.UCI);
            }
            sw.Close();
            sw.Dispose();
            File.WriteAllText(@"D:\Downloads\Syngentha\Pruebas\Prueba_6.txt", "Total de lineas: " + grupos.Count());
        }









        //[TestMethod]
        //public void testmethod2()
        //{
        //    string archivo = @"d:\downloads\syngentha\pruebas\prueba_0.csv";
        //    List<CamposHelper> listado = new List<CamposHelper>();
        //    var lectura = File.ReadLines(archivo);
        //   var total= lectura.Count();
        //    foreach (var l in lectura)
        //    {
        //        var splitted = l.Split('|');
        //        if (splitted[0] != "1")
        //        {
        //            Console.WriteLine(splitted[0]);
        //        }
        //    }
        //}



        //[TestMethod]
        //public void TestMethod7()
        //{
        //    DateTime FechaElegida = Convert.ToDateTime("2022-01-20");
        //    string Archivo = @"D:\Downloads\Syngentha\Pruebas\220122COVID19MEXICO.csv";
        //    List<CamposHelper> Listado = new List<CamposHelper>();
        //    var lectura = File.ReadLines(Archivo);
        //    int contador = 0;
        //    foreach (var a in lectura)
        //    {
        //        if (contador == 0)
        //        {
        //            contador++;
        //            continue;
        //        }
        //        contador++;
        //        var spliteado = a.Split(',');
        //        CamposHelper Caso = new CamposHelper();
        //        Caso.FECHA_ACTUALIZACION = spliteado[0].Replace("\"", "");
        //        Caso.ID_REGISTRO = spliteado[1].Replace("\"", "");
        //        Caso.SECTOR = spliteado[3].Replace("\"", "");
        //        Caso.ENTIDAD_UM = spliteado[4].Replace("\"", "");
        //        Caso.SEXO = spliteado[5].Replace("\"", "");
        //        Caso.ENTIDAD_RES = spliteado[7].Replace("\"", "");
        //        Caso.MUNICIPIO_RES = spliteado[8].Replace("\"", "");
        //        Caso.TIPO_PACIENTE = spliteado[9].Replace("\"", "");
        //        Caso.FECHA_INGRESO = spliteado[10].Replace("\"", "");
        //        Caso.FECHA_SINTOMAS = spliteado[11].Replace("\"", "");
        //        Caso.FECHA_DEF = spliteado[12].Replace("\"", "");
        //        Caso.INTUBADO = spliteado[13].Replace("\"", "");
        //        Caso.NEUMONIA = spliteado[14].Replace("\"", "");
        //        Caso.EDAD = spliteado[15].Replace("\"", "");
        //        Caso.EMBARAZO = spliteado[17].Replace("\"", "");
        //        Caso.DIABETES = spliteado[20].Replace("\"", "");
        //        Caso.EPOC = spliteado[21].Replace("\"", "");
        //        Caso.ASMA = spliteado[22].Replace("\"", "");
        //        Caso.INMUSUPR = spliteado[23].Replace("\"", "");
        //        Caso.HIPERTENSION = spliteado[24].Replace("\"", "");
        //        Caso.OTRA_COM = spliteado[25].Replace("\"", "");
        //        Caso.CARDIOVASCULAR = spliteado[26].Replace("\"", "");
        //        Caso.OBESIDAD = spliteado[27].Replace("\"", "");
        //        Caso.RENAL_CRONICA = spliteado[28].Replace("\"", "");
        //        Caso.TABAQUISMO = spliteado[29].Replace("\"", "");
        //        Caso.TOMA_MUESTRA_LAB = spliteado[31].Replace("\"", "");
        //        Caso.RESULTADO_LAB = spliteado[32].Replace("\"", "");
        //        Caso.TOMA_MUESTRA_ANTIGENO = spliteado[33].Replace("\"", "");
        //        Caso.RESULTADO_ANTIGENO = spliteado[34].Replace("\"", "");
        //        Caso.CLASIFICACION_FINAL = spliteado[35].Replace("\"", "");
        //        Caso.UCI = spliteado[39].Replace("\"", "");
        //        Listado.Add(Caso);
        //    }
        //    StreamWriter sw = new StreamWriter(@"D:\Downloads\Syngentha\Pruebas\Prueba_6.csv");
        //    sw.Close();
        //    sw.Dispose();
        //}
    }
}
